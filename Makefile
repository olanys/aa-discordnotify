appname = aa-discordnotify
package = discordnotify

help:
	@echo "Makefile for $(appname)"

coverage:
	coverage run ../myauth/manage.py test $(package).tests --keepdb --failfast && coverage html && coverage report -m

test:
	# runs a full test incl. re-creating of the test DB
	python ../myauth/manage.py test $(package) --failfast --debug-mode -v 2

check_complexity:
	flake8 $(package) --max-complexity=10

flake8:
	flake8 $(package) --count
